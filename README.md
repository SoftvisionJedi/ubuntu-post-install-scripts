# Ubuntu-Post-Install-Scripts

This is a helper bash script for installing a wide variety of full stack, Java, and .NET developer tools.

> NOTE: This is meant to be run on an Ubuntu Desktop Environment

> Edit the config.sh file for setting up your machine specific parameters.

## Include scripts are:

Name | Description
---- | -----------
Docker | Virtual Containers to run services in.
NodeJS | Javascript based back-end service.
Java 8 | The Oracle Java JDK and JRE.
.NET | Microsoft's .Net Core and Runtime.
Mono | Open-Source Mono Runtime with Mono Develop IDE
