#!/bin/bash

source scripts/_parameters.sh

npm_apps=(
	"npm"
	"create-react-app"
	"express-generator"
	"grunt-cli" 
	"yarn"
	"gulp-cli"
	"ember-cli"
	"@angular/cli"
	"yo"
	"live-server"
	"typescript"
	"tslint"
	"jslint"
	"eslint"
	"jshint"
	"webpack"
	"babel"
	"electrode-ignite"
	"jsdoc"
	"http-server"
	"auto-install"
	"public-ip" 
	"is-up" 
	"is-online" 
	"speed-test"
	"fkill"
) 

python_packages=(
  "python-dev" 
  "python-pip"
  "python-virtualenv"
  "python-numpy" 
  "python-matplotlib"
  "python3"
  "python3-dev" 
  "python3-pip"
  "python3-virtualenv"
  "python3-numpy" 
  "python3-matplotlib"
)

apt_packages=(
  "lsb-core"
  "software-properties-common"
  "openssh-server"
  "ca-certificates"
  "wget"
  "curl"
  "git"
  "make"
  "gcc"
  "g++"
  "screen"
  "build-essential"
  "cmake"
  "ubuntu-restricted-extras"
  "unzip"
  "dialog"
  "apt-transport-https"
  "libavahi-compat-libdnssd-dev"
  "gdebi-core"
  "maven"
  "flashplugin-installer"
  "vlc"
  "ffmpeg"
  "ubuntu-make"
)

program_is_installed() {
  # set to 1 initially
  local return_=1
  # set to 0 if not found
  type $1 >/dev/null 2>&1 || { local return_=0; }
  # return value
  return $return_
}

npm_package_is_global_installed() {
	if npm list -g $1 | grep -q $1; then 
		return 0
	else 
		return 1
	fi
}

# return 1 if local npm package is installed at ./node_modules, else 0
# example
# echo "gruntacular : $(npm_package_is_installed gruntacular)"
npm_package_is_installed() {
  local return_=1
  ls node_modules | grep $1 >/dev/null 2>&1 || { local return_=0; }
  return return_
}

# args: 
# input - $1
machine_has() {
    hash "$1" > /dev/null 2>&1
    return $?
}

# args:
# input - $1
remove_trailing_slash() {
    local input="${1:-}"
    echo "${input%/}"
    return 0
}

# args:
# input - $1
remove_beginning_slash() {
    local input="${1:-}"
    echo "${input#/}"
    return 0
}

function install_docker_ce {
  echo '--------------------------------------------------------'
  echo 'Install Docker CE'
  sudo touch /etc/apt/sources.list.d/docker.list
  echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu artful stable" | sudo tee -a /etc/apt/sources.list.d/docker.list
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo apt-get update
  sudo apt-get install docker-ce
  sudo docker --version
  #mkdir ~/.docker
  #sudo groupadd docker
  #sudo usermod -aG docker $USER
  #sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
  #Ssudo chmod g+rwx "/home/$USER/.docker" -R
}

install_docker_io(){
  sudo apt install docker.io docker-composer docker-doc
  sudo systemctl start docker
  sudo systemctl enable docker
  sudo docker run hello-world
}

# compiler development
# sudo apt install flex bison

# install nosql database
# sudo apt install mongodb-server
# install sqlite3 for smaller db
# sudo apt install libsqlite3-dev sqlite3
# mysql community version installation
# sudo apt install mysql-server libmysqlclient-dev

# Install apache2 and php
# sudo apt install apache2 apache2-utils libapache2-mod-php
# sudo apt install php php-dev php-mcrypt php-mysql php-mbstring php-dom
# Enable mod_rewrite for apache2
# sudo a2enmod rewrite

# umake web visual-studio-code
# umake ide arduino
# umake ide webstorm
# umake web visual-studio-code --remove 

pre_install() {
  #sudo killall update-notifier
  sudo dpkg --configure -a

  echo '--------------------------------------------------------'
  echo 'Preform A System Update'
  sudo apt update

  echo '--------------------------------------------------------'
  echo 'Preform A System Upgrade'
  sudo apt -y dist-upgrade

  echo '--------------------------------------------------------'
  echo 'Clean Up'
  sudo apt -y autoremove

  echo '--------------------------------------------------------'
  echo 'Pre-Install Completed'
  echo '--------------------------------------------------------'
  echo ' ' 
}

install_software (){
  echo '--------------------------------------------------------'
  echo 'Install Additional Software'

  for app in "${apt_packages[@]}"
  do
    sudo apt -y install $app
  done
}

install_additional_fonts(){
  echo '--------------------------------------------------------'
  echo 'Install Microsoft Fonts'
  sudo apt-get install ttf-mscorefonts-installer -y
  sudo fc-cache -f -v
}

install_java8() {
  echo '--------------------------------------------------------'
  echo 'Add Java 8'
  sudo add-apt-repository ppa:webupd8team/java
  sudo apt-get update
  sudo apt-get install oracle-java8-installer
  javac -version

  echo "JAVA_HOME=\"/usr/lib/jvm/java-8-oracle\"" | sudo tee -a /etc/environment
  source /etc/environment
  echo $JAVA_HOME

  echo '--------------------------------------------------------'
  echo 'Installation Completed'
  echo '--------------------------------------------------------'
  echo ' ' 
}

install_ms_net(){
  echo '--------------------------------------------------------'
  echo 'Install Microsoft .net Core and Mono'

  wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.asc.gpg
  sudo mv microsoft.asc.gpg /etc/apt/trusted.gpg.d/
  wget -q https://packages.microsoft.com/config/ubuntu/18.04/prod.list 
  sudo mv prod.list /etc/apt/sources.list.d/microsoft-prod.list

  #curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
  #sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
 
  sudo apt update

  sudo apt-key adv --keyserver packages.microsoft.com --recv-keys EB3E94ADBE1229CF
  sudo apt-key adv --keyserver packages.microsoft.com --recv-keys 52E16F86FEE04B979B07E28DB02C46DF417A0893
  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF

  sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-ubuntu-bionic-prod bionic main" > /etc/apt/sources.list.d/dotnetdev.list'

  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
  echo "deb https://download.mono-project.com/repo/ubuntu stable-bionic main" | sudo tee /etc/apt/sources.list.d/mono-official-stable.list
  
  sudo apt update

  sudo apt -y install mono-devel mono-complete mono-dbg ca-certificates-mono referenceassemblies-pcl libcurl3 libicu55 libicu57
  sudo apt -y install dotnet-sdk-2.1.2* monodevelop monodevelop-nunit monodevelop-versioncontrol monodevelop-database

  sudo apt autoremove 

  echo '--------------------------------------------------------'
  echo 'Installation Completed'
  echo '--------------------------------------------------------'
  echo ' ' 
}

install_system_nodejs() {
  curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
  sudo apt-get -y update
  sudo apt-get install -y nodejs
}

install_nvm(){
  if program_is_installed "nvm"; then
		echo "NVM already installed"
		sleep 1s
  else
  	curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
  	command -v nvm
  fi
}

install_npm_apps(){
for app in "${npm_apps[@]}"
do
	if npm_package_is_global_installed "$app"; then
		echo "Global Package Exists  --> $app"
	else
		echo "Global Package Missing --> $app"
		npm i -g $app
	fi
done
  #sudo npm i -g trash cpy 
  #sudo npm i -g wifi-password  wallpaper  

 
}

install_nodejs(){
	clear
	echo "$bash_header"

	install_nvm
	install_npm_apps
}

pre_install

install_software

install_system_nodejs
install_nvm
install_npm_apps
install_ms_net
install_java8
install_additional_fonts

pre_install