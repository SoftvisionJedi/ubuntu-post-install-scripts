#!/bin/bash

source scripts/_parameters.sh
source scripts/_helpers.sh

source scripts/config.sh
source scripts/common.sh
source scripts/docker.sh
source scripts/java.sh
source scripts/net_mono.sh
source scripts/nodejs.sh

function standard {
	echo ""
}

function all {
	echo ""
}

function menu {
	echo "$bash_header"
	echo "$menu_title"
	select opt in "${menu_options[@]}" "Quit"; 
	do
		case "$REPLY" in
			1 ) standard ;;
			2 ) ;;
			3 ) install_nodejs ;;
			4 ) ;;
			5 ) ;;
			6 ) install_software ;;
			7 ) all ;;
			
			$(( ${#menu_options[@]}+1 )) ) menu_run=$[ $menu_run + 1 ] ;;
			
			*) echo "Invalid option. Try another one."; sleep 1s;;

		esac
		sleep 1s
		break
	done
}

echo "$bash_header"

if [ $(uname) = 'Linux' ]; then
  echo "Linux OS Detected"
else
  echo "Linux Based OS Required"
  exit
fi


echo 'Need To Do Some House Cleaning First...'
pre_install

while [ $menu_run -eq 1 ]
do
	clear
	menu
done
clear


