# BASH SCRIPT CHEAT SHEET

## Bash Script Standard Variables

Name | Description
---- | -----------
`$0` | The name of the Bash script.
`$1 - $9` | The first 9 arguments to the Bash script.
`$#` | How many arguments were passed to the Bash script.
`$@` | All the arguments supplied to the Bash script.
`$?` | The exit status of the most recently run process.
`$$` | The process ID of the current script.
`$USER` | The username of the user running the script.
`$HOSTNAME` | The hostname of the machine the script is running on.
`$SECONDS` | The number of seconds since the script was started.
`$REPLY` | Returns the input from the user.
`$RANDOM` | Returns a different random number each time is it referred to.
`$LINENO` | Returns the current line number in the Bash script.
`$PATH` | Returns a list of environment variables
`$HOME` | The home directory of the user executing this script.
`$(command)` | Grab the output of the given command.
`${variable}` | # Display the output of the command that is stored in a variable.

## Conditional Statements

```bash

if [ -z "$string" ]; then
  echo "String is empty"
elif [ -n "$string" ]; then
  echo "String is not empty"
else
  echo "Something else here"
fi

```

### Multiple Conditionals

and - &&

or - ||

### Arguments to pass into the conditional statement

Option | Type | Description
------ | --------- | -----------
-n | STRING | The length of STRING is greater than zero.
-z | STRING | The length of STRING is zero (ie it is empty).
-d | FILE | FILE exists and is a directory.
-e | FILE | FILE exists.
-r | FILE | FILE exists and the read permission is granted.
-s | FILE | FILE exists and it's size is greater than zero (ie. it is not empty).
-w | FILE | FILE exists and the write permission is granted.
-x | FILE | FILE exists and the execute permission is granted.
-nt | FILE | FILE 1 is newer than FILE 2
-ot | FILE | FILE 1 is older than FILE 2
-ef | FILE | FILE 1 is the same as FILE 2

## Comparers

Operator | Type(s) | Description
-------- | ------- | -----------
! | STRING or NUM | The EXPRESSION is false.
= | STRING or NUM | STRING1 is equal to STRING2
!= | STRING 0r NUM | STRING1 is not equal to STRING2
-eq | NUM | NUM 1 is numerically equal to NUM 2
-gt | NUM | INTEGER1 is numerically greater than INTEGER2
-lt | NUM | INTEGER1 is numerically less than INTEGER2

## Other useful commands

```bash

PWD  # Current working directory

```

## UNAME Command

Usage: `uname [OPTION(s)]`

Print certain system information.  With no OPTION, same as -s.

Option | Extended | Description
------ | -------- | -----------
-a | --all | print all information, in the following order, except omit -p and -i if unknown:
-s | --kernel-name | print the kernel name
-n | --nodename | print the network node hostname
-r | --kernel-release | print the kernel release
-v | --kernel-version | print the kernel version
-m | --machine | print the machine hardware name
-p | --processor | print the processor type (non-portable)
-i | --hardware-platform | print the hardware platform (non-portable)
-o |--operating-system | print the operating system
<na> | --help | display this help and exit
<na> | --version | output version information and exit