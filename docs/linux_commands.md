# Linux Commands

> Most commands will display helpful information if you suffix the given command with `--help`

## Common Commands

Name | Description | Example
---- | ----------- | -------
apt | Advanced package tool for the Debian based system | `sudo apt update`
cal | Used to displays calendar of the present month. | `cal`
cat | Join plain files and/or print contents | `cat abcd.txt`
cd | Change directory. | `cd /home` , `cd ..`
chmod | Changes the file mode (permission) of a file, folder, etc... | `chmod 777 abc.sh`
chown | Change file owner and group of a file, folder, etc... | `chown server:server Binary`
clear | Clears the terminal screen. | `clear`
cp | Copies a file from one location to another. | `cp ~/Downloads abc.tar.gz ~/Desktop`
date | Set or print the current date and time | `date --set='28 february 2018 08:10'`
dd | Used to convert and copy a file. | `dd if=/home/user/debian.iso of=/dev/sdb1 bs=512M; sync`
df | Displays the file system disk space usage | `df -h`
diff | Compare to files. | `diff -w abc.txt def.txt`
find | Find files using file-name. | `find -iname "abc.txt"`
free | Display the free, used, swap memory available
grep | Search for a given string in a file. | `grep -i "the" abc`
history | Prints the history of long list of executed commands
kill | Terminate a process. | `kill -9 7243`
locate | Meant to find a file within the Linux OS. | `locate abc.txt`
ls | List the contents of a directory. | `ls -la`
lsblk | Print block devices by their assigned name | `lsblk -J`
mv | Allows a user to move a file to another folder or directory.
mkdir | Allows the user to make a new directory.
pwd | Prints the current working directory with full path
rm | Allows the user to remove files and directories (option -fR).
rmdir | Allows the user to remove an existing directory.
sort | Sort a file in a specified order. | `sort -r abc.txt`
sudo |  (super user do) execute a command as the superuser
tar | Tape Archive is useful in creation of archive. | `tar -zxvf abc.tar.gz `
top | Displays the top processes in the system | `top -u node`
touch | Allows users to make empty files.
uname | Print detailed information about the machine.
which | Display the path of the provided command
whoami | Display the logged in user name

## Uncommon Commands

Name | Description | Example
---- | ----------- | -------
at | Run a particular command, time based.
convert | Convert an image
expr | Perform simple math calculations.
factor | Possible factors of a decimal number.
id | Print User and Group Id.
last | History of last logged in users.
look | Check for a word from the dictionary.
lsb_release | Prints distribution specification information.
nl | Outputs the content of text file with lines numbered.
pdftk | Concatenate pdf files into single file.
pstree | Prints running processes with child processes.
pv | Outputs simulating text. (Not in OS standard install)
rsync | Simple Back-up utility
shuf | Randomly selects line/file/folder from a file/folder.
strace | A debugging tool.
ss | Outputs Socket Statistics.
stat | Shows the status information of a file.
tac | Prints content of a file, in reverse order.
tree | Prints files and folders in tree like.
yes | continues to print a sting, till interrupt.

## Advance Commands

Name | Description
---- | -----------
visudo | Edits the sudoers file in a safe fashion.

## More Commands

 ```bash

# Display Date/Time in upper right corner of terminal: 
while sleep 1;do tput sc;tput cup 0 $(($(tput cols)-29));date;tput rc;done &

# Show animated digital clock at the prompt.
watch -t -n1 “date +%T|figlet”

# Run a command in background, even after terminal session is closed.
disown -a && exit command

# Outputs all the service/process using port 80.
lsof -iTCP:80 -sTCP:LISTEN

# Check if a specific port is open or not.
nc -ZV localhost port_number

# Outputs Geographical Information, regarding an ip_address.
curl ipinfo.io

# Lists all files owned by user ‘abc’
find .-user abc

# Outputs Processes and Threads of a user.
ps -LF -u user_name

# Outputs the size of all the files and folders within current folder, in human readable format.
du -h –max-depth=1 command

# This simple scripts, opens up, unread mail of an user, in the terminal itself.
curl -u gmail_id –silent “https://mail.google.com/mail/feed/atom” | perl -ne ‘print “\t” if //; print “$2\n” if /(.*)/;’

# Graphically display numbe of connections for each hosts.
netstat -an | grep ESTABLISHED | awk '{print $5}' | awk -F: '{print $1}' | sort | uniq -c | awk '{ printf("%s\t%s\t",$2,$1) ; for (i = 0; i < $1; i++) {printf("*")}; print "" }'

# Display all application currently using the Internet
lsof -P -i -n | cut -f 1 -d " "| uniq | tail -n +2


```