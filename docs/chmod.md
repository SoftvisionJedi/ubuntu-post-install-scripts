# CHMOD - Changing Properties of a file

Command : `chmod [options] mode[,mode] file1 [file2 ...]`

Typical implemented options:

Option | Name |Description
------ | ---- | -----------
-R | recursive | include objects in subdirectories
-f | force | force ahead with all objects even if errors occur
-v | verbose | show objects processed

## Numeric Permissions

Value | Equivalent | Description
----- | ---------- | -----------
0 | - - - | ***no access***
1 | - - x | ***execute***
2 | - w - | ***write***
3 | - w x | ***write / execute***
4 | r - - | ***read***
5 | r - x | ***read / execute***
6 | r w - | ***read / write***
7 | r w x | ***read / write / execute***


## References 

Reference |	Class	| Description
--------- | ----- | -----------
u	| owner	| file's owner
g	| group	| users who are members of the file's group
o	| others | users who are neither the file's owner nor members of the file's group
a	| all	all | three of the above, same as ugo

The chmod program uses an operator to specify how the modes of a file should be adjusted. The following operators are accepted:

Operator	Description
- \+ adds the specified modes to the specified classes
- \- removes the specified modes from the specified classes
- \= the modes specified are to be made the exact modes for the specified classes


Other Values

- u = user
- g = group
- o = other (not user or group)
- a = all
- \+ = add permissions
- = remove permissions
- r = read
- w = write
- x = execute
- t = sticky bit

#### Examples

```sh

chmod 777 filename.ext
chmod 775 filename.ext
chmod +x filename.ext    # Make the file executable

```