#!/bin/bash

#########################################################################################################################
# 

# Stop script on NZEC
set -e

# Stop script if unbound variable found (use ${var:-} if intentional)
set -u

# By default cmd1 | cmd2 returns exit code of cmd2 regardless of cmd1 success
# This is causing it to fail
set -o pipefail

#########################################################################################################################
# 

SUPPORT_MAP="
x86_64-debian-wheezy
x86_64-debian-jessie
x86_64-debian-stretch
x86_64-debian-buster
x86_64-ubuntu-trusty
x86_64-ubuntu-xenial
x86_64-ubuntu-bionic
x86_64-ubuntu-artful
"

#########################################################################################################################
# 

hostname=$(uname -n)
kernel_version=$(uname -r)
kernel_name=$(uname -s)
machine=$(uname -m)

#########################################################################################################################
# 

current_menu="Main Menu"

bash_header=" 
-------------------------------------------------------------
-------------------------------------------------------------
--------   UBUNTU/DEBIAN POST INSTALLTION UTILITY    --------
-------------------------------------------------------------
-------------------------------------------------------------
"

menu_header="
-------------------------------------------------------------
$current_menu

"

#########################################################################################################################
# 

menu_run=1;

menu_title="Software Installation Options"
menu_prompt="Select an option:"
menu_options=("Standard" "Docker" "NodeJS" ".NET Core and Mono" "Java 8" "Common Tools" "All Options")
PS3="$menu_prompt "