#!/bin/bash

install_java8() {
  echo '--------------------------------------------------------'
  echo 'Add Java 8'
  sudo add-apt-repository ppa:webupd8team/java
  sudo apt-get update
  sudo apt-get install oracle-java8-installer
  javac -version

  echo "JAVA_HOME=\"/usr/lib/jvm/java-8-oracle\"" | sudo tee -a /etc/environment
  source /etc/environment
  echo $JAVA_HOME

  echo '--------------------------------------------------------'
  echo 'Installation Completed'
  echo '--------------------------------------------------------'
  echo ' ' 
}