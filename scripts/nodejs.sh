#!/bin/bash

source scripts/_helpers.sh

install_system_nodejs() {
  
  curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
  sudo apt-get -y update
  sudo apt-get install -y nodejs
  
  echo '--------------------------------------------------------'
  echo 'Installation Completed'
  echo '--------------------------------------------------------'
  echo ' ' 
}

install_nvm(){
  if program_is_installed "nvm"; then
		echo "NVM already installed"
		sleep 1s
  else
  	curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
  	command -v nvm
  fi
}

npm_apps=(
	"npm"
	"create-react-app"
	"express-generator"
	"grunt-cli" 
	"yarn"
	"gulp-cli"
	"ember-cli"
	"@angular/cli"
	"yo"
	"live-server"
	"typescript"
	"tslint"
	"jslint"
	"eslint"
	"jshint"
	"webpack"
	"babel"
	"electrode-ignite"
	"jsdoc"
	"http-server"
	"auto-install"
	"public-ip" 
	"is-up" 
	"is-online" 
	"speed-test"
	"fkill"
) 

install_npm_apps(){
for app in "${npm_apps[@]}"
do
	if npm_package_is_global_installed "$app"; then
		echo "Global Package Exists  --> $app"
	else
		echo "Global Package Missing --> $app"
		npm i -g $app
	fi
done
  #sudo npm i -g trash cpy 
  #sudo npm i -g wifi-password  wallpaper  

 
}

install_nodejs(){
	clear
	echo "$bash_header"

	install_nvm
	install_npm_apps
}