#!/bin/bash

install_ms_net(){
  echo '--------------------------------------------------------'
  echo 'Install Microsoft .net Core and Mono'

  wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.asc.gpg
  sudo mv microsoft.asc.gpg /etc/apt/trusted.gpg.d/
  wget -q https://packages.microsoft.com/config/ubuntu/18.04/prod.list 
  sudo mv prod.list /etc/apt/sources.list.d/microsoft-prod.list

  #curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
  #sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
 
  sudo apt update

  sudo apt-key adv --keyserver packages.microsoft.com --recv-keys EB3E94ADBE1229CF
  sudo apt-key adv --keyserver packages.microsoft.com --recv-keys 52E16F86FEE04B979B07E28DB02C46DF417A0893
  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF

  sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-ubuntu-bionic-prod bionic main" > /etc/apt/sources.list.d/dotnetdev.list'

  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
  echo "deb https://download.mono-project.com/repo/ubuntu stable-bionic main" | sudo tee /etc/apt/sources.list.d/mono-official-stable.list
  
  sudo apt update

  sudo apt -y install mono-devel mono-complete mono-dbg ca-certificates-mono referenceassemblies-pcl libcurl3 libicu55 libicu57
  sudo apt -y install dotnet-sdk-2.1.2* monodevelop monodevelop-nunit monodevelop-versioncontrol monodevelop-database

  sudo apt autoremove 

  echo '--------------------------------------------------------'
  echo 'Installation Completed'
  echo '--------------------------------------------------------'
  echo ' ' 
}