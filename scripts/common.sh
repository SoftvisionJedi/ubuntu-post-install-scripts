#!/bin/bash

source scripts/_parameters.sh
source scripts/_helpers.sh
source scripts/config.sh

python_packages=(
  "python-dev" 
  "python-pip"
  "python-virtualenv"
  "python-numpy" 
  "python-matplotlib"
  "python3"
  "python3-dev" 
  "python3-pip"
  "python3-virtualenv"
  "python3-numpy" 
  "python3-matplotlib"
)

apt_packages=(
  "lsb-core"
  "software-properties-common"
  "openssh-server"
  "ca-certificates"
  "wget"
  "curl"
  "git"
  "make"
  "gcc"
  "g++"
  "screen"
  "build-essential"
  "cmake"
  "ubuntu-restricted-extras"
  "unzip"
  "dialog"
  "apt-transport-https"
  "libavahi-compat-libdnssd-dev"
  "gdebi-core"
  "maven"
  "flashplugin-installer"
  "vlc"
  "ffmpeg"
  "ubuntu-make"
)

# compiler development
# sudo apt install flex bison

# install nosql database
# sudo apt install mongodb-server
# install sqlite3 for smaller db
# sudo apt install libsqlite3-dev sqlite3
# mysql community version installation
# sudo apt install mysql-server libmysqlclient-dev

# Install apache2 and php
# sudo apt install apache2 apache2-utils libapache2-mod-php
# sudo apt install php php-dev php-mcrypt php-mysql php-mbstring php-dom
# Enable mod_rewrite for apache2
# sudo a2enmod rewrite

# umake web visual-studio-code
# umake ide arduino
# umake ide webstorm
# umake web visual-studio-code --remove 

pre_install() {
  #sudo killall update-notifier
  sudo dpkg --configure -a

  echo '--------------------------------------------------------'
  echo 'Preform A System Update'
  sudo apt update

  echo '--------------------------------------------------------'
  echo 'Preform A System Upgrade'
  sudo apt -y dist-upgrade

  echo '--------------------------------------------------------'
  echo 'Clean Up'
  sudo apt -y autoremove

  echo '--------------------------------------------------------'
  echo 'Pre-Install Completed'
  echo '--------------------------------------------------------'
  echo ' ' 
}

install_software (){
  echo '--------------------------------------------------------'
  echo 'Install Additional Software'

  for app in "${apt_packages[@]}"
  do
    sudo apt -y install $app
  done
}

install_additional_fonts(){
  echo '--------------------------------------------------------'
  echo 'Install Microsoft Fonts'
  sudo apt-get install ttf-mscorefonts-installer -y
  sudo fc-cache -f -v
}
