function install_docker_ce {
  echo '--------------------------------------------------------'
  echo 'Install Docker CE'
  sudo touch /etc/apt/sources.list.d/docker.list
  echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu artful stable" | sudo tee -a /etc/apt/sources.list.d/docker.list
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo apt-get update
  sudo apt-get install docker-ce
  sudo docker --version
  mkdir ~/.docker
  sudo groupadd docker
  sudo usermod -aG docker $USER
  sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
  sudo chmod g+rwx "/home/$USER/.docker" -R
}

install_docker_io(){
  sudo apt install docker.io docker-composer docker-doc
  sudo systemctl start docker
  sudo systemctl enable docker
  sudo docker run hello-world
}