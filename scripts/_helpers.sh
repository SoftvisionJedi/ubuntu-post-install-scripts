#!/bin/bash

program_is_installed() {
  # set to 1 initially
  local return_=1
  # set to 0 if not found
  type $1 >/dev/null 2>&1 || { local return_=0; }
  # return value
  return $return_
}

npm_package_is_global_installed() {
	if npm list -g $1 | grep -q $1; then 
		return 0
	else 
		return 1
	fi
}

# return 1 if local npm package is installed at ./node_modules, else 0
# example
# echo "gruntacular : $(npm_package_is_installed gruntacular)"
npm_package_is_installed() {
  local return_=1
  ls node_modules | grep $1 >/dev/null 2>&1 || { local return_=0; }
  return return_
}

# args: 
# input - $1
machine_has() {
    hash "$1" > /dev/null 2>&1
    return $?
}

# args:
# input - $1
remove_trailing_slash() {
    local input="${1:-}"
    echo "${input%/}"
    return 0
}

# args:
# input - $1
remove_beginning_slash() {
    local input="${1:-}"
    echo "${input#/}"
    return 0
}